function HA = test(~)


%% Generated on 27-Apr-2018

%---------Automaton created from Component 'ArtificialPancreas'------------

%% Interface Specification:
%   This section clarifies the meaning of state & input dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (ArtificialPancreas):
%  state x := [X; Isc1; Isc2; Gp; Gt; Gs; Il; Ip; I1; Id; t]
%  input u := [uc]

%---------------------Component ArtificialPancreas-------------------------

%----------------------------State loc_I1_t1-------------------------------

%% equation:
%   X'== -0.0278 * X +0.0278 * ( 18.2129 * Ip - 100.25) & 
%               Isc1'== -0.0171 * Isc1 + 0.97751710655*0.05 &
%               Isc2'== 0.0152 * Isc1 - 0.0078 * Isc2 &
%               Gp'== 4.7314 - 0.0047 * Gp - 0.0121 * Id - 1 - 0.0581*Gp + 0.0871*Gt + (1.140850553428184e-4)*t^2*uc + (6.134609247812877e-5)*t*uc &
%               Gt'== -0.0039* (3.2267+0.0313*X) * Gt * ( 1 - 0.0026 * Gt + (2.5097e-6) *Gt^2) + 0.0581 *Gp - 0.0871* Gt &
%               Gs'== 0.1*( 0.5221 * Gp - Gs) &
%               Il'== -0.4219 * Il + 0.2250* Ip &
%               Ip'== -0.3150 * Ip + 0.1545 * Il + 0.0019 * Isc1 + 0.0078 * Isc2 &
%               I1'== -0.0046 * ( I1 - 18.2129 * Ip) &
%               Id'== -0.0046 * (Id - I1 ) &
%               t'==1
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(11,1,@test_St1_FlowEq,dynOpt); 

%% equation:
%   Gs<=80 & t>=0 & t<=30
invA = ...
[0,0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,-1;0,0,0,0,0,0,0,0,0,0,1];
invb = ...
[80;-0;30];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{1} = location('S1',1, inv, trans, dynamics);



%----------------------------State loc_I2_t1-------------------------------

%% equation:
%   X'== -0.0278 * X +0.0278 * ( 18.2129 * Ip - 100.25) & 
%               Isc1'== -0.0171 * Isc1 + 0.97751710655*0.1 &
%               Isc2'== 0.0152 * Isc1 - 0.0078 * Isc2 &
%               Gp'== 4.7314 - 0.0047 * Gp - 0.0121 * Id - 1 - 0.0581*Gp + 0.0871*Gt + (1.140850553428184e-4)*t^2*uc + (6.134609247812877e-5)*t*uc &
%               Gt'== -0.0039* (3.2267+0.0313*X) * Gt * ( 1 - 0.0026 * Gt + (2.5097e-6) *Gt^2) + 0.0581 *Gp - 0.0871* Gt &
%               Gs'== 0.1*( 0.5221 * Gp - Gs) &
%               Il'== -0.4219 * Il + 0.2250* Ip &
%               Ip'== -0.3150 * Ip + 0.1545 * Il + 0.0019 * Isc1 + 0.0078 * Isc2 &
%               I1'== -0.0046 * ( I1 - 18.2129 * Ip) &
%               Id'== -0.0046 * (Id - I1 ) &
%               t'==1
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(11,1,@test_St2_FlowEq,dynOpt); 

%% equation:
%   Gs>=75 & Gs<=120 & t>=0 & t<=30
invA = ...
[0,0,0,0,0,-1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,-1;0,...
0,0,0,0,0,0,0,0,0,1];
invb = ...
[-75;120;-0;30];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{2} = location('S2',2, inv, trans, dynamics);



%----------------------------State loc_I3_t1-------------------------------

%% equation:
%   X'== -0.0278 * X +0.0278 * ( 18.2129 * Ip - 100.25) & 
%               Isc1'== -0.0171 * Isc1 + 0.97751710655*0.2 &
%               Isc2'== 0.0152 * Isc1 - 0.0078 * Isc2 &
%               Gp'== 4.7314 - 0.0047 * Gp - 0.0121 * Id - 1 - 0.0581*Gp + 0.0871*Gt + (1.140850553428184e-4)*t^2*uc + (6.134609247812877e-5)*t*uc &
%               Gt'== -0.0039* (3.2267+0.0313*X) * Gt * ( 1 - 0.0026 * Gt + (2.5097e-6) *Gt^2) + 0.0581 *Gp - 0.0871* Gt &
%               Gs'== 0.1*( 0.5221 * Gp - Gs) &
%               Il'== -0.4219 * Il + 0.2250* Ip &
%               Ip'== -0.3150 * Ip + 0.1545 * Il + 0.0019 * Isc1 + 0.0078 * Isc2 &
%               I1'== -0.0046 * ( I1 - 18.2129 * Ip) &
%               Id'== -0.0046 * (Id - I1 ) &
%               t'==1
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(11,1,@test_St3_FlowEq,dynOpt); 

%% equation:
%   Gs>=115 & Gs<=180 & t>=0 & t<=30
invA = ...
[0,0,0,0,0,-1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,-1;0,...
0,0,0,0,0,0,0,0,0,1];
invb = ...
[-115;180;-0;30];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{3} = location('S3',3, inv, trans, dynamics);



%----------------------------State loc_I4_t1-------------------------------

%% equation:
%   X'== -0.0278 * X +0.0278 * ( 18.2129 * Ip - 100.25) & 
%               Isc1'== -0.0171 * Isc1 + 0.97751710655*0.5 &
%               Isc2'== 0.0152 * Isc1 - 0.0078 * Isc2 &
%               Gp'== 4.7314 - 0.0047 * Gp - 0.0121 * Id - 1 - 0.0581*Gp + 0.0871*Gt + (1.140850553428184e-4)*t^2*uc + (6.134609247812877e-5)*t*uc &
%               Gt'== -0.0039* (3.2267+0.0313*X) * Gt * ( 1 - 0.0026 * Gt + (2.5097e-6) *Gt^2) + 0.0581 *Gp - 0.0871* Gt &
%               Gs'== 0.1*( 0.5221 * Gp - Gs) &
%               Il'== -0.4219 * Il + 0.2250* Ip &
%               Ip'== -0.3150 * Ip + 0.1545 * Il + 0.0019 * Isc1 + 0.0078 * Isc2 &
%               I1'== -0.0046 * ( I1 - 18.2129 * Ip) &
%               Id'== -0.0046 * (Id - I1 ) &
%               t'==1
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(11,1,@test_St4_FlowEq,dynOpt); 

%% equation:
%   Gs>=175 & Gs<=300 & t>=0 & t<=30
invA = ...
[0,0,0,0,0,-1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,-1;0,...
0,0,0,0,0,0,0,0,0,1];
invb = ...
[-175;300;-0;30];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{4} = location('S4',4, inv, trans, dynamics);



%----------------------------State loc_I5_t1-------------------------------

%% equation:
%   X'== -0.0278 * X +0.0278 * ( 18.2129 * Ip - 100.25) & 
%               Isc1'== -0.0171 * Isc1 + 0.97751710655*0.5 &
%               Isc2'== 0.0152 * Isc1 - 0.0078 * Isc2 &
%               Gp'== 4.7314 - 0.0047 * Gp - 0.0121 * Id - 1 - 0.0581*Gp + 0.0871*Gt + (1.140850553428184e-4)*t^2*uc + (6.134609247812877e-5)*t*uc &
%               Gt'== -0.0039* (3.2267+0.0313*X) * Gt * ( 1 - 0.0026 * Gt + (2.5097e-6) *Gt^2) + 0.0581 *Gp - 0.0871* Gt &
%               Gs'== 0.1*( 0.5221 * Gp - Gs) &
%               Il'== -0.4219 * Il + 0.2250* Ip &
%               Ip'== -0.3150 * Ip + 0.1545 * Il + 0.0019 * Isc1 + 0.0078 * Isc2 &
%               I1'== -0.0046 * ( I1 - 18.2129 * Ip) &
%               Id'== -0.0046 * (Id - I1 ) &
%               t'==1
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(11,1,@test_St5_FlowEq,dynOpt); 

%% equation:
%   Gs>=295 & t>=0 & t<=30
invA = ...
[0,0,0,0,0,-1,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,-1;0,0,0,0,0,0,0,0,0,0,1];
invb = ...
[-295;-0;30];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{5} = location('S5',5, inv, trans, dynamics);



HA = hybridAutomaton(loc);


end