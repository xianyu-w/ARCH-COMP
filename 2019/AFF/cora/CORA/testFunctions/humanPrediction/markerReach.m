function [B_vel, B_acc] = markerReach(s_0, v_0, s_0_delta,v_0_delta,v_max,a_max,t)
% markerReach - computes reachable sets of a marker on the human body
%
% This is mostly taken out of the thesis of A. Pereira "Guaranteeing Safe
% Robot Motion"
% 
%
% Syntax:  
%    B = markerReach(s_0, v_0, s_0_delta,v_0_delta,a_max,t)
%
% Inputs:
%    s_0 - initial position
%    v_0 - initial velocity
%    s_0_delta - radius of initial position uncertainty
%    v_0_delta - radius of initial velocity uncertainty
%    v_max - maximum velocity
%    a_max - maximum acceleration
%    t - vector of points in time
%
% Outputs:
%    B - cell array of reachable balls
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% dimension
dim = length(s_0);

% initial ball
B_s0 = capsule(s_0,[],s_0_delta);

% init B
B_vel = cell(length(t),1);
B_acc = cell(length(t),1);

for i=1:length(t)
    % final ball for velocity
    B_vel{i} = B_s0 + capsule(zeros(dim,1),[],v_max*t(i));
    
    % ball due to initial velocity set; 
    B_v0 = capsule(v_0*t(i),[],v_0_delta*t(i));
    % ball due to tue limited acceleration
    B_aMax = capsule(zeros(dim,1),[],0.5*a_max*t(i)^2);
    % final ball for acceleration
    B_acc{i} = B_s0 + B_v0 + B_aMax;
end

%------------- END OF CODE --------------