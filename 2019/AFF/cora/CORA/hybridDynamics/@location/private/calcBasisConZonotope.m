function D = calcBasisConZonotope(obj,R,Rset,Pguard,options)
% calcBasisConZonotope - calculate suitable orthogonal transformation 
%                        matrices for the tight overapproximation of the 
%                        sets R with intervals
%
% Syntax:  
%    D = calcBasisConZonotope(obj,R,Rset,Pguard,options)
%
% Inputs:
%    obj - location object
%    R - cell array of reachable sets after intersection
%    R - cell array of reachable sets before intersection
%    Pguard - guard set that was intersected by the reachable sets
%    options - struct containing algorithm options
%
% Outputs:
%    D - cell array containing the different transformation matrices
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Niklas Kochdumper
% Written:      23-May-2018 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % initialization
    D = cell(length(options.enclosureEnables),1);
    
    % loop over all selected options
    for i = 1:length(options.enclosureEnables)
        
        switch options.enclosureEnables(i)
           
            % axis aligned bounding box
            case 1
                D{i} = eye(obj.contDynamics.dim);
                
                
                
            % Principal Component Analysis (generators)
            case 2
                
                % extract hyperplane normal vector
                if isa(Pguard,'halfspace')
                   n = get(Pguard,'c'); 
                elseif isa(Pguard,'constrainedHyperplane')
                   n = get(Pguard.h,'c');
                end
                
                % concatenate all generators
                G = extractGenerators(Rset);
                
                % project the generators onto the hyperplane
                if isa(Pguard,'halfspace') || isa(Pguard,'constrainedHyperplane')
                    
                   % normalize normal vector
                   n = n./norm(n);
                   
                   % projection
                   G = G - n * n'*G;
                end
                
                % limit maximum number of generators to 1000
                if size(G,2) > 1000
                    [~,ind] = sort(sum(G.^2,1),'descend');
                    G = G(:,ind(1:1000));
                end
                
                % calcualte an orthogonal transformation matrix using PCA
                [D{i},~,~] = svd(G); 
            
                
                
            % Principal Component Analysis (mid points)
            case 3
                
                % extract hyperplane normal vector
                if isa(Pguard,'halfspace')
                   n = get(Pguard,'c'); 
                elseif isa(Pguard,'constrainedHyperplane')
                   n = get(Pguard.h,'c');
                end
                
                % concatenate all zonotope centers
                c = extractMidPoints(Rset);
                
                % project the zonotope centers onto the hyperplane
                if isa(Pguard,'halfspace') || isa(Pguard,'constrainedHyperplane')
                   
                    % normalize normal vector
                    n = n./norm(n);
                    
                    % projection
                    c = c - n * n'*c;
                end
                
                % calcualte an orthogonal transformation matrix using PCA
                [D{i},~,~] = svd(c);  
                
                
                
            % Principal Component Analysis (generators overapprox. conZono)
            case 4
                
                G = [];
                
                if strcmp(options.guardIntersect,'conZonotopeFast')
                
                    % intersections are represented as zonotopes
                    for j = 1:length(R)

                        if ~iscell(R{j})        % sets are not split

                            G = extractIntersectedGenerators(R{j},G);

                        else                    % sets are split

                            for k = 1:length(R{j})

                                G = extractIntersectedGenerators(R{j}{k},G);                     
                            end
                        end
                    end
                    
                else
                    
                    % intersections are represented as constraint zonotopes
                    for j = 1:length(R)

                        if ~iscell(R{j})        % sets are not split

                            % over-approximate the constrained zonotope
                            zono = zonotope(R{j});

                            % extract the generators
                            G = [G,zono.Z(:,2:end)];

                        else                    % sets are split

                            for k = 1:length(R{j})

                                % over-approximate the constrained zonotope
                                zono = zonotope(R{j}{k});

                                % extract the generators
                                G = [G,zono.Z(:,2:end)];                 
                            end
                        end
                    end 
                end
                
                % limit maximum number of generators to 1000
                if size(G,2) > 1000
                    [~,ind] = sort(sum(G.^2,1),'descend');
                    G = G(:,ind(1:1000));
                end
                
                % calcualte an orthogonal transformation matrix using PCA
                [D{i},~,~] = svd(G); 
                          
                
                
                
            % Principal Component Analysis (mid points overapprox. conZono)
            case 5
                
                c = [];
                
                if strcmp(options.guardIntersect,'conZonotopeFast')
                
                    % intersections are represented as zonotopes
                    for j = 1:length(R)

                        if ~iscell(R{j})        % sets are not split

                            c = extractIntersectedMidPoints(R{j},c);

                        else                    % sets are split

                            for k = 1:length(R{j})

                                c = extractIntersectedMidPoints(R{j}{k},c);                     
                            end
                        end
                    end
                    
                else
                    
                    % intersections are represented as constraint zonotopes
                    for j = 1:length(R)

                        if ~iscell(R{j})        % sets are not split

                            % over-approximate the constrained zonotope
                            zono = zonotope(R{j});

                            % extract the generators
                            c = [c,zono.Z(:,1)];

                        else                    % sets are split

                            for k = 1:length(R{j})

                                % over-approximate the constrained zonotope
                                zono = zonotope(R{j}{k});

                                % extract the generators
                                c = [c,zono.Z(:,1)];                 
                            end
                        end
                    end 
                end
                
                % calcualte an orthogonal transformation matrix using PCA
                [D{i},~,~] = svd(c); 
        end        
    end
end



% Auxiliary Functions -----------------------------------------------------

function G = extractGenerators(R)
% extract all generator vectors from the sets that are over-approximated

    G = [];

    % loop over all sets
    for j = 1:length(R)
       if iscell(R{j})              % sets are split
          for k = 1:length(R{j})
              if isa(R{j}{k},'zonotopeBundle')
                    for i = 1:R{j}{k}.parallelSets
                        G = [G,R{j}{k}.Z{i}.Z(:,2:end)];
                    end
              else
                    G = [G,R{j}{k}.Z(:,2:end)];
              end
          end
       else                         % sets are not split
          if isa(R{j},'zonotopeBundle')
                for i = 1:R{j}.parallelSets
                    G = [G,R{j}.Z{i}.Z(:,2:end)];
                end
          else
                G = [G,R{j}.Z(:,2:end)];
          end
       end
    end
end


function G = extractIntersectedGenerators(R,G)
% extract all generators from the zonotope-overapproximation of the
% intersection with the hyperplane

    if isfield(R,'intersection')
        
        % loop over all parallel sets
        for i = 1:length(R.intersection)
            G = [G,R.intersection{i}.Z(:,2:end)];  
        end
    else
        G = [G,R.Z(:,2:end)];          
    end
end


function c = extractMidPoints(R)
% extract all generator vectors from the sets that are over-approximated

    c = [];

    % loop over all sets
    for j = 1:length(R)
       if iscell(R{j})              % sets are split
          for k = 1:length(R{j})
              if isa(R{j}{k},'zonotopeBundle')
                    for i = 1:R{j}{k}.parallelSets
                        c = [c,R{j}{k}.Z{i}.Z(:,1)];
                    end
              else
                    c = [c,R{j}{k}.Z(:,1)];
              end
          end
       else                         % sets are not split
          if isa(R{j},'zonotopeBundle')
                for i = 1:R{j}.parallelSets
                    c = [c,R{j}.Z{i}.Z(:,1)];
                end
          else
                c = [c,R{j}.Z(:,1)];
          end
       end
    end
end


function c = extractIntersectedMidPoints(R,c)
% extract all mid points from the zonotope-overapproximation of the
% intersection with the hyperplane

    if isfield(R,'intersection')
        
        % loop over all parallel sets
        for i = 1:length(R.intersection)
            c = [c,R.intersection{i}.Z(:,1)];  
        end
    else
        c = [c,R.Z(:,1)];          
    end
end

%------------- END OF CODE --------------