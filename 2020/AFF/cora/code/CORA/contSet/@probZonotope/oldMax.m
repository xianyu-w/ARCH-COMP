function res = oldMax(probZ,m)
% max - Computes an overapproximation of the maximum on the m-sigma bound
%
% Syntax:  
%    res = max(probZ,m)
%
% Inputs:
%    probZ - probabilistic zonotope object
%    m - m of the m-sigma bound
%
% Outputs:
%    res - overapproximated maximum value
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       22-August-2007
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

%load generator matrix
G=1e-4*probZ.g;
%get dimension and number of generators
[d,nrOfGens]=size(G);
%generate zero mean zonotope from generators
Z=zonotope(zeros(d,1),G);

value = zeros(nrOfGens,1);
for iGen=1:nrOfGens
    %find nonzero sigmas
    indices=find(probZ.variance(:,iGen));
    %compute auxiliary value
    auxValue=sum(probZ.omega(indices,iGen)./probZ.variance(indices,iGen));
    %compute factors 
    value(iGen)=auxValue*2*norm(probZ.g(:,iGen));
end

a=nrOfGens-d+1;
res=prod(value)*exp(-0.5*m^2)^a/(sqrt(2*pi))^nrOfGens/volume(Z);

%------------- END OF CODE --------------