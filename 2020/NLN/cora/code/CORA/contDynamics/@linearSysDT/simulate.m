function [t,x,ind] = simulate(obj,params,varargin)
% simulate - simulates a linear discrete-time system
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params)
%    [t,x,ind] = simulate(obj,params,odeOpts)
%
% Inputs:
%    obj - nonlinearSysDT object
%    params - struct containing the parameters for the simulation
%       .tStart: initial time
%       .tFinal: final time
%       .timeStep: time step size
%       .x0: initial point
%       .u: piecewise constant input signal u(t) specified as a matrix
%           for which the number of rows is identical to the number of
%           system input
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    t - time vector
%    x - state vector
%    ind - returns the event which has been detected
%
% Example:
%    A = [1 2; -3 1];
%    B = [2;1];
%    sys = linearSysDT('twoDimSys',A,B);
%
%    options.timeStep = 1;
%    options.u = [0.1 0.05 0.05 -0.1];
%
%    [t,x] = simulate(sys,options,0,4,[0;0]);
%
%    plot(x(:,1),x(:,2),'.k','MarkerSize',20);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: nonlinearSysDT/simulate

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      20-March-2020
% Last update:  24-March-2020 (NK)
%               08-May-2020 (MW, update interface)
% Last revision:---

%------------- BEGIN CODE --------------

% set initial state
t = params.tStart:params.timeStep:params.tFinal;
x = zeros(length(t),length(params.x0));

x(1,:) = params.x0';

% consider changing inputs
if size(params.u,2) ~= 1
    change = 1;
else
    change = 0; 
end

% get constant offset
if isempty(obj.c)
   c = zeros(size(obj.A,1),1); 
end

% loop over all time steps
for i = 1:length(t)-1

    if change
        temp = obj.A*x(i,:)' + obj.B * params.u(:,i) + c;
    else
        temp = obj.A*x(i,:)' + obj.B * params.u + c;
    end

    x(i+1,:) = temp';
end

ind = [];
    
%------------- END OF CODE --------------